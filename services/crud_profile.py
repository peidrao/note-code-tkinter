import bcrypt

from db.db_session import create_session
from models.profile import Profile
from tkinter import messagebox
from utils.helpers import clear_text, set_password, verify_email, verify_email


def create_profile(first_name, last_name, email, password) -> None:
    if first_name.get() == "":
        return messagebox.showerror(
            title="First name is empty", message="You cannot leave the first name empty"
        )
    if not verify_email(email.get()):
        return messagebox.showerror(
            title="E-mail invalid", message="Your email is invalid"
        )
    if password.get() == "" or len(password.get()) <= 3:
        return messagebox.showerror(
            title="Password is too short",
            message="Your password needs 3 more characters",
        )

    profile: Profile = Profile(
        first_name=first_name.get(),
        last_name=last_name.get(),
        email=email.get(),
        password=set_password(password.get()),
    )

    with create_session() as session:
        session.add(profile)
        session.commit()

    messagebox.showinfo(title="Success", message="User was created successfully")
    clear_text(first_name, last_name, email, password)


def login_profile(input_email, input_pass):

    if not verify_email(input_email.get()):
        return messagebox.showerror(
            title="E-mail invalid", message="Your email is invalid"
        )

    with create_session() as session:
        profile: Profile = (
            session.query(Profile).filter(Profile.email == input_email.get()).first()
        )

    if not profile:
        return messagebox.showerror(title="Not found", message="Email doesnt exists")

    if input_pass.get() == "":
        return messagebox.showerror(title="Error", message="Passwords is empty")

    if bcrypt.checkpw(input_pass.get().encode("utf-8"), profile.password):
        return True
    else:
        return messagebox.showerror(title="Error", message="Passwords not match")
