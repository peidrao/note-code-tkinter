import tkinter as tk
import ttkbootstrap as ttk
from ttkbootstrap.constants import *
from ttkbootstrap import Notebook


class DashBoardMain(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        container = tk.Frame(self)
        window_width = 600
        window_height = 400

        offset_left = int((self.winfo_screenwidth() - window_width) / 2)
        offset_top = int((self.winfo_screenheight() - window_height) / 2)

        self.geometry(f"{window_width}x{window_height}+{offset_left}+{offset_top}")
        container.pack(side="top", fill="both", expand=True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (DashBoardPage, CreateNotePage):
            frame = F(container, self)
            self.frames[F] = frame
            frame.grid(row=0, column=0, sticky="nsew")
        self.show_frame(DashBoardPage)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()

    def destroy_frames(self):
        for i in self.frames:
            i.quit(self)


class DashBoardPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        b1 = ttk.Button(self, text='New Note', bootstyle=PRIMARY, command=lambda: controller.show_frame(CreateNotePage))
        b2 = ttk.Button(self, text='My Notes', bootstyle=SECONDARY)
        button_quit = ttk.Button(self, text='Quit', bootstyle=DANGER, command=lambda: controller.destroy_frames())

        b1.pack(side=LEFT, anchor='n')
        b2.pack(side=LEFT, anchor='n')
        button_quit.pack(side=RIGHT, anchor='ne', )


class CreateNotePage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.show_create_note_page(controller)

    def show_create_note_page(self, controller):
        frame_title = tk.Frame(self)
        frame_body = tk.Frame(self)

        label_email = ttk.Label(frame_title, text="Title")
        input_email = ttk.Entry(frame_title, width=52)
        label_email.pack(side=LEFT, anchor='w', expand=True, padx=(10, 38))
        input_email.pack(side=RIGHT, anchor='e', expand=True)

        label_body = ttk.Label(frame_body, text="Message")
        input_body = ttk.Text(frame_body, height=5, width=52)
        label_body.pack(side=LEFT, anchor='w', expand=True, padx=(10, 10), )
        input_body.pack(side=RIGHT, anchor='e', expand=True)

        frame_title.pack(side=TOP, anchor='nw', pady=(5, 10))
        frame_body.pack(side=TOP, anchor='w')

        submit = ttk.Button(self, text="Create", bootstyle=SUCCESS)
        submit.pack(side=LEFT, anchor='e', expand=True, padx=(0, 10))
        main_page = ttk.Button(self, text="Return", bootstyle=DANGER,
                               command=lambda: controller.show_frame(DashBoardPage))
        main_page.pack(anchor='center', expand=True)
        main_page.pack(side=RIGHT, anchor='w', expand=True)
