import tkinter as tk
from pages.dashboard import DashBoardMain
import ttkbootstrap as ttk
from ttkbootstrap.constants import *

from services.crud_profile import create_profile, login_profile


class AppMain(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        container = tk.Frame(self)
        window_width = 600
        window_height = 400

        offset_left = int((self.winfo_screenwidth() - window_width) / 2)
        offset_top = int((self.winfo_screenheight() - window_height) / 2)

        self.geometry(f"{window_width}x{window_height}+{offset_left}+{offset_top}")
        container.pack(side="top", fill="both", expand=True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (MainPage, SignupPage, LoginPage):
            frame = F(container, self)
            self.frames[F] = frame
            frame.grid(row=0, column=0, sticky="nsew")
        self.show_frame(MainPage)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()

    def destroy_frames(self):
        for i in self.frames:
            i.quit(self)


class MainPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        main = ttk.Label(self, text="Manager Profiles", font=("", 25))
        main.pack()

        signup = ttk.Button(self, text="Sign Up", command=lambda: controller.show_frame(SignupPage))
        signup.pack(side=LEFT, anchor='e', expand=True)

        login = ttk.Button(self, text="Login", command=lambda: controller.show_frame(LoginPage))
        login.pack(side=RIGHT, anchor='w', expand=True)

        quit_button = ttk.Button(self, text="Quit", bootstyle="danger", command=lambda: controller.destroy_frames())
        quit_button.pack(side=BOTTOM, padx=5, pady=10)


class LoginPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.show_login_page()

    def show_login_page(self):
        main = ttk.Label(self, text="Login", font=("", 25))
        main.pack()

        ttk.Label(self, text="E-mail").pack(pady=(50, 0))
        input_email = ttk.Entry(self)
        input_email.pack()
        ttk.Label(self, text="Password").pack()
        input_password = ttk.Entry(self)
        input_password.config(show="*")
        input_password.pack()

        submit = ttk.Button(self, text="Login", command=lambda: self.profile_page(input_email, input_password))
        submit.pack(side=LEFT, anchor='e', expand=True)

        main_page = tk.Button(self, text="MainPage", command=lambda: self.controller.show_frame(MainPage))
        main_page.pack(side=RIGHT, anchor='w', expand=True)

    def profile_page(self, input_email, input_pass):
        login_profile(input_email, input_pass)
        self.controller.destroy()
        DashBoardMain().mainloop()


class SignupPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        ttk.Label(self, text="Signup", font=("", 25)).pack(pady=(0, 20))
        self.show_signup_page()

    def show_signup_page(self):
        ttk.Label(self, text="First Name").pack()
        input_fname = tk.Entry(self)
        input_fname.pack()

        ttk.Label(self, text="Last Name").pack()
        input_lname = tk.Entry(self)
        input_lname.pack()

        ttk.Label(self, text="E-mail").pack()
        input_email = tk.Entry(self)
        input_email.pack()

        ttk.Label(self, text="Password").pack()
        input_pass = tk.Entry(self)
        input_pass.config(show="*")
        input_pass.pack()

        submit = tk.Button(self, text="Submit", command=lambda: create_profile(
            input_fname, input_lname, input_email, input_pass))
        submit.pack(side=LEFT, anchor='e', expand=True)

        main_page = tk.Button(self, text="MainPage", command=lambda: self.controller.show_frame(MainPage))
        main_page.pack(side=RIGHT, anchor='w', expand=True)
