from sqlalchemy import Column, Integer, DateTime, String
from datetime import datetime
from db.model_base import ModelBase


class Profile(ModelBase):
    __tablename__: str = "profiles"

    id: int = Column(Integer, primary_key=True)
    first_name: str = Column(String(45), nullable=False)
    last_name: str = Column(String(45), nullable=False)
    email: str = Column(String(45), nullable=False)
    password: str = Column(String(100), nullable=False)

    created_at: datetime = Column(DateTime, default=datetime.now, index=True)

    def __repr__(self) -> str:
        return f"<Profile: {self.first_name}>"
