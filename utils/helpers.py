import re

import bcrypt


def clear_text(fname, lname, email, password):
    fname.delete(0, "end")
    lname.delete(0, "end")
    email.delete(0, "end")
    password.delete(0, "end")


def verify_email(email):
    pat = "([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+"
    if re.match(pat, email):
        return True
    return False


def set_password(password):
    salt = bcrypt.gensalt()
    hash = bcrypt.hashpw(password.encode("utf-8"), salt)
    return hash
